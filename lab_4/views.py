from django.shortcuts import render
from lab_2.models import Notes
from .forms_note import NoteForm
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    note = Notes.objects.all()  # TODO Implement this
    response = {'notes': note }
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}

    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
    
    # check if form data is valids
    if form.is_valid():
        # save the form data to model
        form.save()
        if request.method == "POST":
            return HttpResponseRedirect('/lab-4')
        

    context['form']= form
    return render(request, 'lab4_form.html', context)

def note_list(request):
    note = Notes.objects.all()  # TODO Implement this
    response = {'notes': note }
    return render(request, 'lab4_note_list.html', response)