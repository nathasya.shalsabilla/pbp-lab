from django import forms
from lab_2.models import Notes

class NoteForm(forms.ModelForm):
    class Meta:
        model = Notes
        fields = "__all__"