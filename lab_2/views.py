from django.shortcuts import render
from datetime import datetime, date
from .models import Notes
from django.http.response import HttpResponse
from django.core import serializers
# Create your views here.

def index(request):
    note = Notes.objects.all()  # TODO Implement this
    response = {'notes': note }
    return render(request, 'lab2.html', response)

def xml(request):
    data = serializers.serialize('xml', Notes.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Notes.objects.all())
    return HttpResponse(data, content_type="application/json")